# TP 1 : E. Les tableaux et les objets littéraux <!-- omit in toc -->

## Sommaire <!-- omit in toc -->
- [E.1. Manipulation des tableaux](#e1-manipulation-des-tableaux)
- [E.2. Les Objets littéraux](#e2-les-objets-littéraux)

## E.1. Manipulation des tableaux

Initialiser un tableau nommé `data` avec 3 chaines de caractères : 'Regina', 'Napolitaine', 'Spicy'. Parcourir ce tableau afin de générer 3 liens sur le modèle de celui réalisé plus haut.

Utilisez les différentes méthodes de parcours des tableaux :
1. Une boucle `for` classique
2. La méthode `Array.forEach` cf. https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/forEach
3. la méthode `Array.map` cf. https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/map accompagnée de la méthode `Array.join` cf. https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/join

Le résultat obtenu sera identique dans les 3 cas :
<a href="images/readme/pizzaland-03.jpg"><img src="images/readme/pizzaland-03.jpg" width="80%"></a>

## E.2. Les Objets littéraux
Modifier le tableau `data` : au lieu de 3 chaînes de caractères, on va maintenant y mettre 3 objets littéraux de la forme suivante :
```js
{
	nom: 'Regina',
	base: 'tomate',
	prix_petite: 5.5,
	prix_grande: 7.5
}
```

Modifier la boucle pour générer un code de ce type :
```html
<article class="media">
	<a href="images/regina.jpg">
		<img src="images/regina.jpg" />
		<section class="infos">
			<h4>Regina</h4>
			<ul>
				<li>Prix petit format : 5.50 €</li>
				<li>Prix grand format : 7.50 €</li>
			</ul>
		</section>
	</a>
</article>
```

Le rendu final devra correspondre à ceci :

<a href="images/readme/pizzaland-04.jpg"><img src="images/readme/pizzaland-04.jpg" width="90%"></a>

## Étape suivante <!-- omit in toc -->
Si tout fonctionne, vous pouvez passer à l'étape suivante : [F. Compiler avec Babel](./F-babel.md)